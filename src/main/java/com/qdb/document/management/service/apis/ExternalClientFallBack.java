package com.qdb.document.management.service.apis;

import com.qdb.document.management.domain.Comment;
import com.qdb.document.management.domain.Post;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class ExternalClientFallBack implements ExternalClient {

    @Override
    public List<Post> findPosts(Long postId) {
        return Collections.emptyList();
    }

    @Override
    public List<Comment> findComments(Long userId) {
        return Collections.emptyList();
    }
}