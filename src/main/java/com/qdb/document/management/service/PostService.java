package com.qdb.document.management.service;

import com.qdb.document.management.domain.Post;
import com.qdb.document.management.repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PostService {

    private final Logger logger = LoggerFactory.getLogger(PostService.class);
    private PostRepository postRepository;

    public PostService(PostRepository postRepository){
        this.postRepository = postRepository;
    }

    public Post savePost(Post post, String documentId) {
        post.setDocumentId(documentId);
        return postRepository.save(post);
    }

    public List<Post> findPostsRelatedToDocument(String documentId) {
        Optional<List<Post>> posts = postRepository.findAllByDocumentId(documentId);
        if (posts.isPresent()) {
            return posts.get();

        } else {
            logger.error("Post not found");
        }
        return Collections.EMPTY_LIST;
    }

    public Post findPost(String uuid) {
        return postRepository.getOne(UUID.fromString(uuid));
    }
}
