package com.qdb.document.management.service.apis;

import com.qdb.document.management.domain.Comment;
import com.qdb.document.management.domain.Post;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "external-service", url = "https://jsonplaceholder.typicode.com/", fallback = ExternalClientFallBack.class)
public interface ExternalClient {

    @GetMapping(value = "/users/{userId}/posts")
    public List<Post> findPosts(@PathVariable Long userId);

    @GetMapping(value = "/post/{postId}/comments")
    public List<Comment> findComments(@PathVariable Long postId);

}