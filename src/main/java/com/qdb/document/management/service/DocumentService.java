package com.qdb.document.management.service;

import com.qdb.document.management.model.Document;
import com.qdb.document.management.repository.DocumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class DocumentService {

    private final Logger logger = LoggerFactory.getLogger(DocumentService.class);

    private Path filesLocation;
    private DocumentRepository documentRepository;


    @PostConstruct
    private void buildEnv(){
        String uploadDirectory = System.getProperty("user.dir");
        this.filesLocation = Paths.get(uploadDirectory).toAbsolutePath().normalize();
    }

    public DocumentService(DocumentRepository documentRepository){
        this.documentRepository = documentRepository;
    }


    public void save(MultipartFile file, String owner) {
        String fileName =  owner + "_" + file.getOriginalFilename();
        Path targetLocation = this.filesLocation.resolve(fileName);
        try {
            Files.copy(file.getInputStream(), targetLocation);
            com.qdb.document.management.domain.Document documentToPersist = new com.qdb.document.management.domain.Document();
            documentToPersist.setFileName(fileName);
            documentToPersist.setOwner(owner);
            documentRepository.save(documentToPersist);

        } catch (IOException e) {
            logger.error("Exception occurred {}", e.getMessage());
        }
    }


    public Boolean deleteDocument(String uuid){
        documentRepository.findByUuid(uuid).ifPresent(document -> {
            Path targetLocation = this.filesLocation.resolve(document.getFileName());
            try {
                Files.delete(targetLocation);
                documentRepository.delete(document);
            } catch (IOException e) {
                logger.error("Exception occurred {}", e.getMessage());
            }
        });
        return true;
    }


    public List<Document>  findAllByUserId(String userID){
        List<Document> documentList = new ArrayList<>();
        logger.info(" ********** " + documentRepository.findAll().size());
        documentRepository.findAllByOwner(userID).ifPresent(documents -> {
            documents.forEach(doc -> {
                Document model = new Document(doc.getUuid(), doc.getFileName(), doc.getOwner());
                documentList.add(model);
            });
        });
       return documentList;
    }

}