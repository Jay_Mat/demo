package com.qdb.document.management.repository;

import com.qdb.document.management.domain.Post;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PostRepository extends JpaRepository<Post, UUID> {

    Optional<List<Post>> findAllByDocumentId(String documentId);

}