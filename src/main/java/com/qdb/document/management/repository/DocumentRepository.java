package com.qdb.document.management.repository;

import com.qdb.document.management.domain.Document;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DocumentRepository extends JpaRepository<Document, String> {

    Optional<Document> findByUuid(String uuid);
    Optional<List<Document>> findAllByOwner(String userId);

}