package com.qdb.document.management.rest;

import com.qdb.document.management.domain.Comment;
import com.qdb.document.management.service.apis.ExternalClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("/api/comments")
public class CommentsRestController {

    private ExternalClient externalClient;

    public CommentsRestController(ExternalClient externalClient){
        this.externalClient = externalClient;
    }

    @GetMapping(value = "/user/post/{postId}")
    public ResponseEntity<List<Comment>> userComments(@PathVariable Long postId) {
        List<Comment> list = externalClient.findComments(postId);

        if (list.isEmpty()) {
            return new ResponseEntity<>(list, HttpStatus.OK);
        }

        return ResponseEntity.noContent().build();
    }
}