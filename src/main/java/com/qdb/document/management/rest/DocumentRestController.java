package com.qdb.document.management.rest;

import com.qdb.document.management.model.Document;
import com.qdb.document.management.model.ResponseMessage;
import com.qdb.document.management.service.DocumentService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController("/api/document")
public class DocumentRestController {

    private DocumentService documentService;

    public DocumentRestController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @PostMapping(value = "/upload/{documentOwner}")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable String documentOwner) {
        String message = "";
        try {
            documentService.save(file, documentOwner);
            message = "File upload successful: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    @DeleteMapping(value = "/remove/{uuid}")
    public ResponseEntity remove(@PathVariable String uuid) {
        Map<String, String> status = null;
        try {
            documentService.deleteDocument(uuid);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/listDocuments/{userID}")
    public ResponseEntity listDocuments(@PathVariable String userID) {
        try {
            List<Document> documents = documentService.findAllByUserId(userID);
            if (documents.size() > 0) {
                return new ResponseEntity<>(documents, HttpStatus.OK);
            }
            return new ResponseEntity<>(documents, HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.EXPECTATION_FAILED);
        }
    }

}