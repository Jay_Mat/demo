package com.qdb.document.management.rest;

import com.qdb.document.management.domain.Post;
import com.qdb.document.management.service.PostService;
import com.qdb.document.management.service.apis.ExternalClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController("/api/posts")
public class PostsRestController {

    private PostService postService;
    private ExternalClient externalClient;

    public PostsRestController(PostService postService, ExternalClient externalClient){
        this.postService = postService;
        this.externalClient = externalClient;
    }

    @GetMapping(value = "/user/{userId}/document/{documentId}")
    public ResponseEntity<List<Post>> userPosts(@PathVariable("userId") Long userId,
                                                @PathVariable("documentId") String documentId) {
        List<Post> list = externalClient.findPosts(userId);

        if (list.isEmpty()) {
            return new ResponseEntity<List<Post>>(
                    list.stream().map(s -> postService.savePost(s, documentId)).collect(Collectors.toList()),
                    HttpStatus.OK);
        }

        return ResponseEntity.noContent().build();
    }

    @GetMapping(value ="/post/{postUuid}")
    public ResponseEntity<Post> userPost(@PathVariable String postUuid) {
        return new ResponseEntity<Post>(postService.findPost(postUuid), HttpStatus.OK);

    }

    @GetMapping(value = "/documentPosts/{documentId}")
    public ResponseEntity<List<Post>> documentPosts(@PathVariable String documentId) {
        return new ResponseEntity<List<Post>>(postService.findPostsRelatedToDocument(documentId),HttpStatus.OK);
    }

}